#include "ads1015_fs.hpp"
#include "spdlog/spdlog.h"
#include <fstream>

ads1015_fs_t::ads1015_fs_t() {
  spdlog::info("Creating device");
  std::ofstream f(ads1015_driver::new_device);
  f << "ads1015 0x48";
  f.close();
}

ads1015_fs_t::~ads1015_fs_t() {
  spdlog::info("Deleting device");
  std::ofstream f(ads1015_driver::delete_device);
  f << "0x48";
  f.close();
}

float ads1015_fs_t::read_a0() {
  std::string val;
  std::ifstream f(ads1015_driver::a0);
  f >> val;
  f.close();
  return static_cast<float>(std::stoi(val)) / 1000.0;
}

float ads1015_fs_t::read_a1() {
  std::string val;
  std::ifstream f(ads1015_driver::a1);
  f >> val;
  f.close();
  return static_cast<float>(std::stoi(val)) / 1000.0;
}

float ads1015_fs_t::read_a2() {
  std::string val;
  std::ifstream f(ads1015_driver::a2);
  f >> val;
  f.close();
  return static_cast<float>(std::stoi(val)) / 1000.0;
}

float ads1015_fs_t::read_a3() {
  std::string val;
  std::ifstream f(ads1015_driver::a3);
  f >> val;
  f.close();
  return static_cast<float>(std::stoi(val)) / 1000.0;
}
