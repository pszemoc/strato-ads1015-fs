#include "ads1015_fs.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include <memory>
#include <zmq.hpp>
#include <libconfig.h++>
#include <csignal>
#include <stdexcept>

class Application {
private:
  zmq::context_t                    context;
  std::shared_ptr<zmq::socket_t>    socket;
  std::shared_ptr<ads1015_fs_t>   ads1015;

  inline static bool running = false;
   static void termination_handler(int) {
    spdlog::info("Termination requested");
    running = false;
  }

  void init_log() {
    auto file_logger = spdlog::basic_logger_mt("ads1015", "/var/log/balloon/ads1015.log");
    spdlog::set_default_logger(file_logger);
    spdlog::default_logger()->flush_on(spdlog::level::level_enum::info);
  }

  void init_signals() {
    std::signal(SIGTERM, Application::termination_handler);
  }

  void init_socket() {
    libconfig::Config cfg;
    try {
      cfg.readFile("/etc/sp-config/sp.cfg");
    } catch (const libconfig::FileIOException &fioex) {
      throw std::runtime_error("I/O error while reading file.");
    } catch (const libconfig::ParseException &pex) {
      std::stringstream ss;
      ss << "Parse error at " << pex.getFile() << ":" << pex.getLine()
            << " - " << pex.getError() << std::endl;
      throw std::runtime_error(ss.str().c_str());
    }

    const libconfig::Setting& root = cfg.getRoot();
    int port;
    if (!root["ads1015"].lookupValue("ads1015_port", port)) {
      throw std::runtime_error("ads1015 port not defined in config.");
    }
    spdlog::info("Config parsed.");

    socket = std::make_shared<zmq::socket_t>(context, ZMQ_PUB);
    if (socket != nullptr) {
      socket->bind("tcp://*:" + std::to_string(port));
    } else {
      throw std::runtime_error("Error while creating socket.");
    }
    spdlog::info("Socket opened at: {}", port);
  }

public:
  Application() : context(1) {}    

  int exec() {
    running = true;
    //init_log();
    init_signals();
    try {
      init_socket();
      ads1015 = std::make_shared<ads1015_fs_t>();
    } catch (const std::exception& e) {
      spdlog::error(e.what());
      running = false;
      return -1;
    }

    while (running) {
      std::this_thread::sleep_for(std::chrono::seconds(1)); 

      try {
        spdlog::info("a0: {}", ads1015->read_a0());
      } catch (const std::exception& e) {
        spdlog::error(e.what());
      }
    }
    return 0;
  }
};
