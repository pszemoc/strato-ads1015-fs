#pragma once

namespace ads1015_driver {
  constexpr char new_device[]         = "/sys/class/i2c-adapter/i2c-1/new_device";
  constexpr char delete_device[]      = "/sys/class/i2c-adapter/i2c-1/delete_device";
  constexpr char a0[]                 = "/sys/class/i2c-adapter/i2c-1/1-0048/in4_input";
  constexpr char a1[]                 = "/sys/class/i2c-adapter/i2c-1/1-0048/in5_input";
  constexpr char a2[]                 = "/sys/class/i2c-adapter/i2c-1/1-0048/in6_input";
  constexpr char a3[]                 = "/sys/class/i2c-adapter/i2c-1/1-0048/in7_input";
}

class ads1015_fs_t {
public:
  ads1015_fs_t();
  ~ads1015_fs_t();

  float read_a0(); 
  float read_a1(); 
  float read_a2(); 
  float read_a3(); 
};